---
title: 博客魔改日记
tags:
- HEXO
- 日记
- 博客
categories: 
- 日记
- Hexo
description: 我博客的更新
keywords: HEXO,日记
cover: https://img.dreamfall.cn/cover/8.webp
date: 2022-08-22 17:54:00
updated: 2022-08-26 21:48:00
swiper_index: 2
sticky: 1
---

是时候要给自己的博客写更新日记了，不然连自己改了什么都不知道~~(我不是鸽子)~~

日记的起点：2022年8月22日，我决定将博客彻底开源之日

> 关于以前的魔改我已无法回忆，就当作这是一次崭新的开始吧

{% folding cyan open, 2022年 %}

{% folding yellow open, 8月 %}

{% timeline 更新日记,blue %}

<!-- timeline 2022-08-19 -->

1. 恢复[github源码](https://github.com/meng-luo/Source-Blog/)的更新，并利用脚本每日上传。
2. 重新激活github actions推送[部署](https://github.com/meng-luo/meng-luo.github.io/)，并同步到[vercel](https://mengluo.vercel.app/)。
3. 更新[追番](https://blog.dreamfall.cn/bangumis/)使用[Bangumi](https://bgm.tv/)源。
4. 搭建友链[朋友圈](https://blog.dreamfall.cn/fcircle/)。
5. 加入评论弹幕的支持
6. 进行宽屏适配

<!-- endtimeline -->

<!-- timeline 2022-08-20 -->

1. 更新[追番](https://blog.dreamfall.cn/bangumis/)的静态资源，通过cdn加快访问速度。
2. 更新依赖

<!-- endtimeline -->

<!-- timeline 2022-08-21 -->

1. 更新友链[朋友圈](https://blog.dreamfall.cn/fcircle/)，采用店长的[Akilar-SAO](https://akilar.top/posts/62f13a97/)方案。
2. 修改字体cdn.

<!-- endtimeline -->

<!-- timeline 2022-08-22 -->

1. 决定开始写更新日记，于是有了本篇文章
2. 修复主页分类磁贴滚动错位

<!-- endtimeline -->

<!-- timeline 2022-08-23 -->

1. 使用sw缓存css和js
2. 新增离线页面，可在断网时访问

<!-- endtimeline -->

<!-- timeline 2022-08-25 -->

1. 增加侧边栏天气
2. 评论系统新增输入提示
3. [关于页](https://blog.dreamfall.cn/about/)新增访客统计

<!-- endtimeline -->

<!-- timeline 2022-08-26 -->

1. 修改主页归档布局
2. 分类标签归档页增加文章索引
3. 标签云增加文章数上下标
4. 修改主页卡片样式

<!-- endtimeline -->
{% endtimeline %}

{% endfolding %}

{% endfolding %}